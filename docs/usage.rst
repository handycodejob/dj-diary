=====
Usage
=====

To use Django Diary in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'dj_diary.apps.DjDiaryConfig',
        ...
    )

Add Django Diary's URL patterns:

.. code-block:: python

    from dj_diary import urls as dj_diary_urls


    urlpatterns = [
        ...
        url(r'^', include(dj_diary_urls)),
        ...
    ]
