=============================
Django Diary
=============================

.. image:: https://badge.fury.io/py/dj-diary.svg
    :target: https://badge.fury.io/py/dj-diary

.. image:: https://travis-ci.org/shenry/dj-diary.svg?branch=master
    :target: https://travis-ci.org/shenry/dj-diary

.. image:: https://codecov.io/gh/shenry/dj-diary/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/shenry/dj-diary

Diary with permissions, useful for tracking

Documentation
-------------

The full documentation is at https://dj-diary.readthedocs.io.

Quickstart
----------

Install Django Diary::

    pip install dj-diary

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'dj_diary.apps.DjDiaryConfig',
        ...
    )

Add Django Diary's URL patterns:

.. code-block:: python

    from dj_diary import urls as dj_diary_urls


    urlpatterns = [
        ...
        url(r'^', include(dj_diary_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage
