============
Installation
============

At the command line::

    $ easy_install dj-diary

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj-diary
    $ pip install dj-diary
