# -*- coding: utf-8 -*-

from django.db import models
from .base import BaseCollection

from model_utils.models import TimeStampedModel




class Week(BaseCollection):
    name = models.CharField(default="Week", max_length=255)
