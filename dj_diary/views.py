# -*- coding: utf-8 -*-
from django.views.generic import (
    CreateView,
    DeleteView,
    DetailView,
    UpdateView,
    ListView
)

from .models import (
	Collection,
	Container,
	Event,
	Journal,
)


class CollectionCreateView(CreateView):

    model = Collection


class CollectionDeleteView(DeleteView):

    model = Collection


class CollectionDetailView(DetailView):

    model = Collection


class CollectionUpdateView(UpdateView):

    model = Collection


class CollectionListView(ListView):

    model = Collection


class ContainerCreateView(CreateView):

    model = Container


class ContainerDeleteView(DeleteView):

    model = Container


class ContainerDetailView(DetailView):

    model = Container


class ContainerUpdateView(UpdateView):

    model = Container


class ContainerListView(ListView):

    model = Container


class EventCreateView(CreateView):

    model = Event


class EventDeleteView(DeleteView):

    model = Event


class EventDetailView(DetailView):

    model = Event


class EventUpdateView(UpdateView):

    model = Event


class EventListView(ListView):

    model = Event


class JournalCreateView(CreateView):

    model = Journal


class JournalDeleteView(DeleteView):

    model = Journal


class JournalDetailView(DetailView):

    model = Journal


class JournalUpdateView(UpdateView):

    model = Journal


class JournalListView(ListView):

    model = Journal

