from django.contrib import admin
from .models.collections import Week
from .models import Container, Collection, Event, Journal, Note


class EventInline(admin.TabularInline):
    readonly_fields = ('modified',)
    model = Event
    extra = 2


class JournalInline(admin.TabularInline):
    readonly_fields = ('modified',)
    model = Journal
    extra = 0

class NoteInline(admin.StackedInline):
    readonly_fields = ("created", "modified",)
    model = Note
    extra = 1
    fieldsets = (
        (None, {
            "fields": (
                "title",
                "text",
                ("read", "created", "modified",),
            ),
        }),
    )

@admin.register(Collection, Week)
class CollectionAdmin(admin.ModelAdmin):
    readonly_fields = ('modified', 'created',)


@admin.register(Container)
class ContainerAdmin(admin.ModelAdmin):
    readonly_fields = ('modified', "created", )
    list_display = ('title', 'start', 'end',)
    ordering = ('start',)
    inlines = [
        JournalInline,
        EventInline,
        NoteInline,
    ]
    fieldsets = (
        (None, {
            "fields": ("title",),
        }),
        ("Times", {
            "fields": (('start', 'end',),),
        }),
        # ("Admin", {
        #     "fields": (("created", "modified"),),
        # }),
    )
