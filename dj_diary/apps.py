# -*- coding: utf-8
from django.apps import AppConfig


class DjDiaryConfig(AppConfig):
    name = 'dj_diary'
