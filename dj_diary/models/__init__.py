# -*- coding: utf-8 -*-

from django.db import models

from .base import BaseContainer, BaseCollection, BaseEvent, BaseJournal, BaseNote


class Collection(BaseCollection):
    pass


class Container(BaseContainer):
    pass


class Journal(BaseJournal):
    container = models.ForeignKey("Container", models.CASCADE, related_name="journals")


class Note(BaseNote):
    container = models.ForeignKey("Container", models.CASCADE, related_name="notes")


class Event(BaseEvent):
    container = models.ForeignKey("Container", models.CASCADE, related_name="events")


__all__ = (
    Container,
    Collection,
    Event,
    Journal,
)
