# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import gettext as _
from django.utils.text import Truncator

from model_utils.models import TimeStampedModel


class BaseCollection(TimeStampedModel):
    name = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    duration = models.DurationField(blank=True, null=True)

    def __str__(self):
        return f"{self.name}: {self.title}, {self.start}-{self.end}"

    class Meta:
        abstract = True


class BaseContainer(TimeStampedModel):
    title = models.CharField(max_length=255, null=True)
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return f"{self.title}: {self.start} - {self.end}"

    class Meta:
        abstract = True


class BaseEvent(TimeStampedModel):
    EATING = "Eating"
    AILMENT = "Ailments"
    EVENT_TYPES = (
        (EATING, _("Eating")),
        (AILMENT, _("Ailment")),
    )

    title = models.CharField(max_length=255)
    type = models.CharField(choices=EVENT_TYPES, max_length=255)
    time = models.DateTimeField(blank=True, null=True)
    desc = models.CharField(max_length=255, blank=True)
    container = models.ForeignKey("BaseContainer", models.CASCADE, related_name="base_events")

    def __str__(self):
        max_len = 32
        desc = Truncator(self.desc).chars(max_len)
        return f"{self.title} ({self.type}): {desc}"

    class Meta:
        abstract = True


class BaseJournal(TimeStampedModel):
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=65536)
    container = models.ForeignKey("BaseContainer", models.CASCADE, related_name="base_journals")

    def __str__(self):
        max_len = 32
        text = Truncator(self.text).chars(max_len)
        return f"{self.title}: {text}"

    class Meta:
        abstract = True


class BaseNote(TimeStampedModel):
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=65536)
    read = models.BooleanField(default=False)
    container = models.ForeignKey("BaseContainer", models.CASCADE, related_name="base_notes")

    def __str__(self):
        max_len = 32
        text = Truncator(self.text).chars(max_len)
        return f"{self.title}: {text}"

    class Meta:
        abstract = True

